%% ��������� ����������������� � ��������������� �����������

figure;
subplot(2,1,1);
hold on;
plot(MDLcurrent);
plot(SGcurrent);
hold off;
grid on;
title('Current');
xlabel('t');
ylabel('I, A');
legend('Model', 'Experiment', 'Location','NorthWest');

subplot(2,1,2);
hold on;
plot(MDLangle);
plot(SGangle);
hold off;
grid on;
title('Angle');
xlabel('t');
ylabel('\phi, rad');
legend('Model', 'Experiment', 'Location','NorthWest');