%% ������������������ ��������� �������������
% 1. ������ ���� - ������������� ���� � ��������
% 2. �������� ��������� - ������������� ����
% 3. �������� ��� - ��������������

%% ��������� � ����������� ������
clc;
open('estimation');
V = 3; % ������ � ����������������� ����������

%% 1. ������ ����
% �������� ������ ��� ������� �����
open_system('estimation');
swNo_load; % �������� ������ �� �������� ����
set_param(gcs,'SimulationCommand','Update'); % ��������� ���������

% �����������
input('1. ���� ������� �����. ���������, ��� ����� �� ������������, � ������� Enter.');
disp('��� ���� ������� �����...');
rtwbuild(gcs); % ��������� �� ������ ��������� �������
tg.start; % ����

% �������� ����� ������������
dt = 0;
while dt < Tend*1.5
    pause(0.2);
    if tg.Status == 'stopped'
        break
    else
        dt = dt + 0.2;
    end
end

% ���������� ������
S_getDataFromSG; % ������ ��������

% ���������
rawSGspeed = diff(rawSGangle) ./ diff(SGtime); % �������� ��� ����������� �� ����
SGspeed = smooth(rawSGspeed, 100);

% ������������
figure('Name', '������ ����: �������������', 'NumberTitle', 'off');
subplot(2,1,1);
plot(SGcurrent);
grid on;
title('���');
xlabel('t');
ylabel('I, A');

subplot(2,1,2);
plot(SGspeed);
grid on;
title('������� ��������');
xlabel('t, �');
ylabel('\omega, ���/�');

%% 2. �������� ���������
% �������� ������ ��� ����� ��������� ���������
open_system('estimation');
swLocked_rotor; % �������� ������ �� ��
set_param(gcs,'SimulationCommand','Update'); % ��������� ���������

% �����������
input('2. ���� ��������� ���������. ����� ������������ ����� ���������� ������������� �����.\n��� ������ ���������� � ������������ ������� Enter.');
disp('��� ���� ��������� ���������...');
rtwbuild(gcs); % ��������� �� ������ ��������� �������
input('������������ ����� � ������� Enter.');
tg.start; % ����

% �������� ����� ������������
dt = 0;
while dt < Tend*1.5
    pause(0.2);
    if tg.Status == 'stopped'
        break
    else
        dt = dt + 0.2;
    end
end

% ���������� ������
S_getDataFromSG; % ������ ��������

% ������������
figure('Name', '�������� ���������: ������������� ����', 'NumberTitle', 'off');
plot(SGcurrent);
grid on;
title('���');
xlabel('t, �');
ylabel('I, A');

%% 3. �������������� ��������� ����
% �������� ������ ��� ������ �������������� ��������� ����
open_system('estimation');
swRamp; % �������� ������ � ����������� ����������
set_param(gcs,'SimulationCommand','Update'); % ��������� ���������

% �����������
input('3. �������������� ��������� ����. ���������, ��� ����� �� ������������, � ������� Enter.');
disp('��� ������ ��������������...');
rtwbuild(gcs); % ��������� �� ������ ��������� �������
tg.start; % ����

% �������� ����� ������������
dt = 0;
while dt < Tend*1.5
    pause(0.2);
    if tg.Status == 'stopped'
        break
    else
        dt = dt + 0.2;
    end
end

% ���������� ������
S_getDataFromSG; % ������ ��������

% ���������
rawSGspeed = diff(rawSGangle) ./ diff(SGtime); % �������� ��� ����������� �� ����
SGspeed = smooth(rawSGspeed, 100);

SGvoltage = SGtime(2:end) / 10;

% ������������
figure('Name', '�������������� ��������� ����', 'NumberTitle', 'off');
plot(SGvoltage, SGspeed);
grid on;
title('������� �������� � ����������� �� ����������');
xlabel('U, �');
ylabel('\omega, ���/�');

%% ���������� ������
clc;
disp('��������� ���������');
