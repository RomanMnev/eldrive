%% ������������� ����������

%% �������� ������ �������
SLVariant       = Simulink.Variant('Plant == 1');
SSCVariant      = Simulink.Variant('Plant == 2');
SGVariant       = Simulink.Variant('Plant == 3');
TFVariant       = Simulink.Variant('Plant == 4');
SPSVariant      = Simulink.Variant('Plant == 5');
% �� ��������� - ������ Simulink
Plant = 1;

%% �������� ������� ����������
StepVariant      = Simulink.Variant('Reference == 1');
CycleVariant     = Simulink.Variant('Reference == 2');
ClockVariant     = Simulink.Variant('Reference == 3');
% �� ��������� - ���������
Reference = 1;

%% �������� ����������
OneLoopVariant      = Simulink.Variant('Regulator == 1');
TwoLoopsVariant     = Simulink.Variant('Regulator == 2');
% �� ��������� - �������������
Regulator = 1;

%% �������� ����������� ������
load bus_data % ����

%% ��������� ������
% ��������� ��������� ���������
D_motor_start;

% ��������� ��������� �����������
D_ctrl_start;

% ������ ���������
D_misc;