%% ��������� ������ � �������� ������������
if (~exist('tg', 'var'))
    error('No target object; aborting.');
end

% ��������� ������ �� �������                                                                        
SGtime = tg.TimeLog;
rawSGoutput = tg.OutputLog;
rawSGcurrent = rawSGoutput(:,1);
rawSGangle = rawSGoutput(:,2);
% ���� ������ ���������� � ����
rawSGangle = (rawSGangle-mean(rawSGangle(1:100)));

% ����������� ��������
rawSGspeed = diff(rawSGangle) ./ diff(SGtime); % �������� ��� ����������� �� ����
smoothSGspeed = smooth(rawSGspeed, 100);

% �������������� �������
SGcurrent = timeseries(rawSGcurrent, SGtime, 'Name', 'SGcurrent');
SGangle = timeseries(rawSGangle, SGtime, 'Name', 'SGangle');
SGspeed = timeseries(smoothSGspeed, SGtime(1:end-1), 'Name', 'SGspeed');

% ������� ��������� ����������
clear rawSGoutput;

% �������� �� �������� ����������
msgbox('������ ��������');